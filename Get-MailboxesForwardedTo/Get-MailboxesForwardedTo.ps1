<#
.SYNOPSIS
  Get list of all mailboxes forwarded to a specific user

.PARAMETER RecipientUsername
  Required parameter.
  Input username to retrieve all mailboxes forwarded to that user

.INPUTS
  See parameter above

.OUTPUTS
  Outputs to console

.NOTES
  Version:        1.0
  Author:         Taylor K. Zuppan
  Creation Date:  02/18/2015
  Purpose/Change: Initial development
  
.EXAMPLE
  Get-MailboxesForwardedTo -RecipientUsername johndoe

  Name                       : Jane Smith
  Alias                      : jsmith
  ForwardingAddress          : domain.local/Users/Jane Smith
  DeliverToMailboxAndForward : False
  ServerName                 : mailserver

  Name                       : Mike Johnson
  Alias                      : mjohnson
  ForwardingAddress          : domain.local/Users/Mike Johnson
  DeliverToMailboxAndForward : True
  ServerName                 : mailserver

.EXAMPLE
  Get-MailboxesForwardedTo johndoe

  Name                       : Jane Smith
  Alias                      : jsmith
  ForwardingAddress          : domain.local/Users/Jane Smith
  DeliverToMailboxAndForward : False
  ServerName                 : mailserver

  Name                       : Mike Johnson
  Alias                      : mjohnson
  ForwardingAddress          : domain.local/Users/Mike Johnson
  DeliverToMailboxAndForward : True
  ServerName                 : mailserver

.EXAMPLE
  Get-MailboxesForwardedTo -RecipientUserName johndoe | FT
  
  Name               Alias         ForwardingAddress             DeliverToMailboxAndForw     ServerName
                                                                                     ard
  ----               -----         -----------------             -----------------------     ----------
  Jane Smith         jsmith        domain.local/Users/J...                         False     mailserver
  Mike Johnson       mjohnson      domain.local/Users/M...                          True     mailserver

.EXAMPLE
  Get-MailboxesForwardedTo johndoe | FT
  
  Name               Alias         ForwardingAddress             DeliverToMailboxAndForw     ServerName
                                                                                     ard
  ----               -----         -----------------             -----------------------     ----------
  Jane Smith         jsmith        domain.local/Users/J...                         False     mailserver
  Mike Johnson       mjohnson      domain.local/Users/M...                          True     mailserver
#>
Add-PSSnapin *exchange*
function Get-MailboxesForwardedTo {
    Param(
        [Parameter(Mandatory=$True,Position=1)]
        [string]$RecipientUsername
    )

    $RecipientIdentity = (Get-Recipient $RecipientUsername).Identity
    Get-Mailbox | Where-Object { $_.ForwardingAddress -eq $RecipientIdentity } | Select Name,Alias,ForwardingAddress,DeliverToMailboxAndForward,ServerName
}