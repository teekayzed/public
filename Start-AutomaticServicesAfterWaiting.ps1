########################################
#    This script will automatically
#    Start services that are set to
#    Automatic start, but are in a 
#    'stopped' state after waiting
#    for 5 minutes in order to provide
#    services that are attempting to
#    start time to finish.
#
#            08/14/2014
#            Taylor K. Zuppan
########################################
Start-Sleep -Seconds 300
Get-WmiObject win32_Service -Filter "startmode = 'auto' and state != 'running'" | ForEach-Object {
    $result = $_.StartService()
    if($result.ReturnValue -ne 0) {
        Write-Host $_.name did NOT start successfully
        Write-Host `r`n`r`n
    } else {
        Write-Host $_.name started successfully
        Write-Host `r`n`r`n
    }
}