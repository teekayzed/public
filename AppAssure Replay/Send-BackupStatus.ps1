#############################################################
# These variables need to be configured for your environment
#---------------------------------------------------------------

#Array of addresses that the report should go to...
$sendmailto = "mailbox@domain.com

#SMTP server we should send mail through
$smtpserver = "smtpserver.domain.com"

# The address the report should show as being from
$mailfromaddress = "replay@domain.com.com"
######################################################################
# Shouldn't need to touch anything below here
#

#######################################################################
# Global Variables necessary for subsequent functions
#----------------------------------------------------------------------
$Log = ""
$ErrorLog = ""
#######################################################################
# !!Needs $ProtectedServers defined outside function
# List protected volumes in an array
# For other functions perform a foreach($Server in $Servers)
#                                                  By: Taylor K. Zuppan
#----------------------------------------------------------------------
$global:ProtectedServers = @()
function Get-ProtectedServer(){
	#Registry Key where Replay Info is supposed to be located
	$Replay_Key= "HKLM:\Software\AppAssure\ReplayEPS"
	#Get an array of the subkeys... should be one for each server being protected
	$Keys = Get-ChildItem $Replay_Key
    #Create an empty array to house the list of protected servers
	Foreach ($Key in $Keys) {
	    #$Key.name is a value like: HKEY_LOCAL_MACHINE\Software\AppAssure\ReplayEPS\mailserver1
	    # so let's split it to get just the server name 
	    $tmparray=$Key.Name.Split("\")
	    #Server's name Should be last element of the array
	    $protectedserverName = $tmpArray[-1]
        $global:ProtectedServers +=  $protectedserverName
    }
}
#######################################################################
# !!Needs: $Log and $ErrorLog defined outside function
# !!Needs: Get-ProtectedServer function to run prior
# !!Usage: Function must be called with
#               Get-RollupStatistic($ProtectedServers)
# Retrieve all rollup settings for each server
# Set variables at the beginning of function in order to configure
#      recommended rollup policy.
# Will warn if rollup policy is not GREATER THAN or EQUAL TO the
#      configured variables.
# Current recommended for cores is as follows: 1, 2, 3, 2, (1 or 2)
# Current recommended for NAS is as follows: 2, 3, 6, 5, 6
#                                                  By: Taylor K. Zuppan
#----------------------------------------------------------------------
function Get-RollupStatistic($Servers){
    $RecommendedAll = 1
    $RecommendedHourly = 2
    $RecommendedDaily = 3
    $RecommendedWeekly = 2
    $RecommendedMonthly = 1

    $Script:Log += "<p><b><u><font size=""+2"">Rollup Statistics</font></u></b><br>"
    $Script:Log += "<u><font size=""+1"">Minimum recommended rollup policy:<br>DaysToKeepAllSnaps: $RecommendedAll<br>DaysToKeepHourlySnaps: $RecommendedHourly<br>DaysToKeepDailySnaps: $RecommendedDaily<br>WeeksToKeepWeeklySnaps: $RecommendedWeekly<br>MonthsToKeepMonthlySnaps: $RecommendedMonthly</font></u><br><br><br>"
    
    Foreach ($Server in $Servers) {
        Try {
            $all = (Get-ItemProperty -Path HKLM:\Software\AppAssure\ReplayEPS\$Server -name days_to_keep_all_snaps).days_to_keep_all_snaps
            $hourly = (Get-ItemProperty -Path HKLM:\Software\AppAssure\ReplayEPS\$Server -name days_to_keep_hourly_snaps).days_to_keep_hourly_snaps
            $daily = (Get-ItemProperty -Path HKLM:\Software\AppAssure\ReplayEPS\$Server -name days_to_keep_daily_snaps).days_to_keep_daily_snaps
            $weekly = (Get-ItemProperty -Path HKLM:\Software\AppAssure\ReplayEPS\$Server -name weeks_to_keep_weekly_snaps).weeks_to_keep_weekly_snaps
            $monthly = (Get-ItemProperty -Path HKLM:\Software\AppAssure\ReplayEPS\$Server -name months_to_keep_monthly_snaps).months_to_keep_monthly_snaps

            If($daily -ne 0) {
	            Write-Host "$Server has Rollups Enabled!`n"
			    $script:LOG += "$Server has Rollups Enabled!`n"
	        }
            else {
	            Write-Host "$Server does not have Rollups Enabled!"
	            $script:LOG += "<p><b><font color=""Red"" size=""+1"">$Server does not have Rollups Enabled!</font></b></P>`n"
			    $script:ERRORS += "<p><b><font color=""Red"" size=""+1"">$Server does not have Rollups Enabled!</font></b></P>`n"
	        }

            Write-Host "Server:`t $Server`nDaysToKeepAllSnaps:`t $all`nDaysToKeepHourlySnaps:`t $hourly`nDaysToKeepDailySnaps:`t $daily`nWeeksToKeepWeeklySnaps:`t $weekly`nMonthsToKeepMonthlySnaps:`t $monthly`n`n"

            if(($all -ge $RecommendedAll) -and ($hourly -ge $RecommendedHourly) -and ($daily -ge $RecommendedDaily) -and ($weekly -ge $RecommendedWeekly) -and ($monthly -ge $RecommendedMonthly)) {
                $Script:Log += "<p>Server:`t $Server<br>"
                $Script:Log += "DaysToKeepAllSnaps:`t $all<br>"
                $Script:Log += "DaysToKeepHourlySnaps:`t $hourly<br>"
                $Script:Log += "DaysToKeepDailySnaps:`t $daily<br>"
                $Script:Log += "WeeksToKeepWeeklySnaps:`t $weekly<br>"
                $Script:Log += "MonthsToKeepMonthlySnaps:`t $monthly</p><br><br>"
            }
            else {
                $Script:Log += "<p><b><font color=""Red""> $Server does not have minimum recommended rollup policy. <br>"
                $Script:Log += "Server:`t $Server<br>"
                $Script:Log += "DaysToKeepAllSnaps:`t $all<br>"
                $Script:Log += "DaysToKeepHourlySnaps:`t $hourly<br>"
                $Script:Log += "DaysToKeepDailySnaps:`t $daily<br>"
                $Script:Log += "WeeksToKeepWeeklySnaps:`t $weekly<br>"
                $Script:Log += "MonthsToKeepMonthlySnaps:`t $monthly</font></b></P><br><br>"
                $Script:ErrorLog += "$Server does not have recommended rollup policy.`n"
            }
        } Catch {
            $Script:Log += "<p><b><font color=""Red"" Size=""+1""> $Server encountered an error when looking up Rollup Statistics.</font></b></p><br><br>"
            $Script:ErrorLog += "$Server encountered an error.`n"
        }
    }
    $Script:Log += "</p>"
}
#######################################################################
# !!Needs: $Log and $ErrorLog defined outside function
# !!Needs: Get-ProtectedServer function to run prior
# !!Usage: Function must be called with 
#               Get-ReplicationStatus($ProtectedServers)
# Retrieve whether or not the protected servers are being replicated
#                                                  By: Taylor K. Zuppan
#----------------------------------------------------------------------
function Get-ReplicationStatus($Servers) {
    $Script:Log += "<p><b><u><font size=""+2"">Replication Status</font></u></b><br>"
    Foreach ($Server in $Servers) {
        Try {
            $replication = (Get-ItemProperty -Path HKLM:\Software\AppAssure\ReplayMirror\ReplicationSources\$Server -name ReplicationEnabled).ReplicationEnabled
            if ($replication -eq "True") {
                $Script:Log += "$Server has replication enabled.<br>"
            }
            else {
                $Script:Log += "<b><font color =""Red"" size=""+1"">$Server DOES NOT have replication enabled.</font></b><br>"
                $Script:ErrorLog += "$Server does not have replication enabled.`n"
            }
        } Catch {
            $Script:Log += "<p><b><font color =""Red"" size=""+1"">$Server encountered an error when looking up Replication Status.</font></b></p>"
        }
    }
    $Script:Log += "</p>"
}
#######################################################################
# !!Needs: $Log and $ErrorLog defined outside function
# !!Needs: Get-ProtectedServer function to run prior
# !!Usage: Function must be called with 
#               Get-ReplicationStatus($ProtectedServers)
#                                                  By: Taylor K. Zuppan
#----------------------------------------------------------------------
function Get-ReplayServiceStatus() {
    $ServiceStatus =  (Get-Service -name ReplayServer64).Status
    if ($ServiceStatus -eq "Running" ) { 
	    $Script:Log += "<p>Replay Service is running</P>`n"
    }
    else { 
	    $Script:Log += "<p><b><font color=""Red"" size=""+1"">Replay server is not Running!</font></b></P>`n"
	    $Script:ErrorLog += "<p><b><font color=""Red"" size=""+1"">Server: $hostname Replay service is not Running!</font></b></P>`n"
    }
}
#######################################################################
# Get Size of files in specified subdirectory in GB
# does _NOT_ recurse
#                                                           By: Unknown
#                                              From original replay.ps1
#----------------------------------------------------------------------
Function Get-SubdirSize([string] $Path){
     $DirSizeBytes = Get-ChildItem $Path | measure-Object length -sum
    [float]$DirSizeMB= $DirSizeBytes.Sum/(1024 * 1024)
    if ($Debug_output){Write-Host "Using $DirSizeMB MB"}
    $DirSizeGB = $DirSizeMB/1024
    if ($Debug_output){Write-Host "Using $DirSizeGB GB" }
    return $DirSizeGB
}
#######################################################################
# !!Needs: Get-SubDirSize()
# Get Disk info about the Archive Disk 
#                                                           By: Unknown
#                                              From original replay.ps1
#----------------------------------------------------------------------
function Get-ProtectedServerDiskInfo($servername,$ArchivePath){
    #Let's get drive letter of where RPs are being stored.
    $Lservername = $servername+"                    " 
    $lservername = $lservername.substring(0,20)
    $ArchiveDrive = $ArchivePath.SubString(0,2)
    if ($Debug_output){ 
        Write-Host "Drive is " $ArchiveDrive
        Write-Host $ArchivePath
    }
    $DirSize= Get-SubDirSize $ArchivePath
    $tmpFilter =  "driveletter='" + $ArchiveDrive + "'"
    $DriveInfo=Get-WmiObject Win32_Volume -filter $tmpFilter
    [float]$DiskSizeGB=[float]$DriveInfo.Capacity/(1024*1024*1024)
    [float]$UsedDiskSpaceGB=([float]$DriveInfo.Capacity -$DriveInfo.FreeSpace)/(1024*1024*1024)
    [float]$FreeDiskSpaceGB=$DisksizeGB-$UsedDiskSpaceGB
    $pctOfUsedGB= $DirSize/$UsedDiskSpaceGB*100
    $pctFree=[float]$DriveInfo.FreeSpace/[float]$DriveInfo.Capacity*100
    $pctUsed=[float]$UsedDiskSpaceGB/[float]$DriveInfo.Capacity*100
    
    Write-Host -nonewline ("$lservername`t$ArchiveDrive`t{0,0:n2}`t{1,5:n2}`t" -f $DirSize,$FreeDiskSpaceGB)
#    $script:LOG+="</p>$lservername`t$ArchiveDrive`t{0,0:n2}`t{1,5:n2}`t" -f $DirSize,$FreeDiskSpaceGB
    $Script:Log += "<u><b>$lservername</b></u><ul><li>Archive Drive $ArchiveDrive Space:<ul><li>Size: $DirSize<li>Free Space: $FreeDiskSpaceGB</ul>"

}
#######################################################################
# !!Needs: Get-ProtVol()
#Runs replayc command line utility and parses the output to get 
# Recovery Point information. 
#                                          From original replay script.
#----------------------------------------------------------------------
function Get-ReplayStatus($ReplayServer,$ProtectedServer){
	#Path to the ReplayC Executable
    $replay_exe='C:\program files (x86)\AppAssure Software\Replay Server\Replayc.exe'
	# check: 
	# for more info about the replayc.exe command
    $replay_out = &$replay_exe /List RPS /core $ReplayServer /protectedserver $ProtectedServer /time all
    # For Testing/debugging on a machine that doesn't have replay installed. We can 
    # use Get-Content and read in a sample output file
    #$replay_out=Get-Content C:\temp\replayc.out
    
    $p = get-protvol $protectedserver
    $pvs = $p.split(";")  
	write-host
#	$script:log+= "<br>"

    foreach ($pv in $pvs){
	    write-host -NoNewline `t`t$pv
	    $script:LOG += "</ul></ul><li>$pv"
	
        $validRPs = 0	#Number of Valid Recovery Points we can find. 
        $invalidRPs= 0	#Number of Invalid Recovery Points we can find. 
        $numRPs = 0		#Number of Recovery points we find
        $numReportedRPs=0 #Number of Recovery Points replayc.exe says exists
        $numInfoLines=0	#Number of what we'll call "informational" lines
        [datetime]$firstValidRP = get-Date	#Pre-fill this with now() so can do a good comparison
        [datetime]$lastValidRP = 0	#Set to epoch to help us get most recent

        foreach ($line in $replay_out){ 
            
            #Find the line where replayc tells us how many recovery points it thinks it has
			If ($line -like "*Total*") { 
                $tmparray=$line.Split()
                $numReportedRPs= $tmparray[-1]
            }
			If ($line.contains($pv)) {
				if ($line -match "^\[VALID\]"){$validRPs++}
				elseif ($line -match "^\[INVALID"){$invalidRPs++}
				
		        $tmpArray=$line.Split()
			    $volume = $tmparray[-1]
	            
				#Build DateTime object from date/time string on this line of output
	            $tmpstr=($tmpArray[1] + " " +  $tmpArray[2] + $tmpArray[3]).TrimEnd(",")

	            $dtm=[DateTime]$tmpstr

				#
	            If ($FirstValidRP.CompareTo($dtm) -gt 0 ){
	                $FirstValidRP = $dtm
	            }
	            If ($lastValidRP.CompareTo($dtm) -lt 0){
	                $LastValidRP = $dtm
	            }
	        }
        }
  
        #What percentage of the RPs are valid
        [float]$validRPpct=[float]$validRPS/[float]$numRPs*100
        #Build our Time Differential String
        $Timediff= $scriptStartTime.Subtract($LastValidRP)
        [string]$TimeDiffString = ""
        If ($TimeDiff.Days -gt 0){ $TimeDiffString += $TimeDiff.Days.ToString() +" Days "} # If this is > 1 there's problem and we should note it
        If ($TimeDiff.Hours -gt 0){ $TimeDiffString += $TimeDiff.Hours.ToString() +" Hrs "}
        If ($TimeDiff.Minutes -gt 0){ $TimeDiffString += $Timediff.Minutes.ToString()+" Min"}

        $numrps = $validRPs+$invalidRPs    
        if ($numReportedRPs -eq $numRPs){ 
            Write-Host -nonewline "$numReportedRPs`t$validRPs`t$invalidRPs`t"
    #        $script:LOG+= "$numReportedRPs`t$validRPs`t$invalidRPs`t"
            $script:Log += "<ul><li>Number of Reported RPs: $numReportedRPs<li>Valid RPs: $validRPs<li>Invalid RPs: $invalidRPs"
        } else {
            Write-Host -NoNewline "$numReportedRPs`t$validRPs`t$invalidRPs`t"
    #        $script:LOG+="<b><font color=""Red"" size=""+1"">$numReportedRPs`t$validRPs`t$invalidRPs`t</b></font>"
            $script:Log += "<ul><li><b><font color=""Red"">Number of Reported RPs: $numReportedRPs<li>Valid RPs: $validRPs<li>Invalid RPs: $invalidRPs</font></b>"
            $script:Errors += "<ul><li><b><font color=""Red"">Number of Reported RPs: $numReportedRPs<li>Valid RPs: $validRPs<li>Invalid RPs: $invalidRPs</font></b>"

        }    
        If ($TimeDiff.TotalHours -gt 24 ){ 
	        write-host "$TimeDiffString"
            $script:LOG+= "<li>Since Last: <b><font color=""Red"">$TimeDiffString</font></b></ul>"
            $script:ERRORS+= "<li>Since Last: <b><font color=""Red"">$protectedserver - $PV - $TimeDiffString</font></b></ul>"
        } else {
    	    write-host "$TimeDiffString"
            $script:LOG+= "<li>Since Last: $TimeDiffString</ul></ul>"
	    }
    }
}
#######################################################################
# List protected volumes in an array
#                                                           By: Unknown
#                                              From original replay.ps1
#----------------------------------------------------------------------
function get-protvol($sname){
    $pvs = ""
    $pgs = get-childitem $replay_key\$protectedservername\protectiongroups\
    foreach ($pg in $pgs) {
	    $tmparray=$pg.Name.Split("\")
	    #Server's name Should be last element of the array
	    $pgname = $tmpArray[-1]
	    $vols = get-childitem $replay_key\$protectedservername\protectiongroups\$pgname\volumes
	    foreach ($vol in $vols) {
		$tmparray=$vol.Name.Split("\")
		#Server's name Should be last element of the array
		$volname = $tmpArray[-1].replace("|","\")
		$pvs = $pvs + $volname + ";"
		}
	}
    return $pvs.trimend(";")
}
#######################################################################
# !!Needs: Get-ProtectedServerDiskInfo() and Get-ReplayStatus
# Compiled main bit from the old replay status script.
#                                                           By: Unknown
#                                              From original replay.ps1
#----------------------------------------------------------------------
function Get-OldReplayPS1(){
    #Registry Key where Replay Info is supposed to be located
    $Replay_Key= "HKLM:\Software\AppAssure\ReplayEPS"

    #Get an array of the subkeys... should be one for each server being protected
    $Keys = Get-ChildItem $Replay_Key

        Write-Host  "SERVER`t`t`tDrive`tUsedGB`tDrv Free`tRPS`tGood`tBad`tLast"
    $Script:Log += "<br><br><p><b><u><font size=""+2"">Recovery Point Statistics</font></u></b><br>"

    #Let's iterate over the list of servers and get some info 
    Foreach ($Key in $Keys) {

        #$Key.name is a value like: HKEY_LOCAL_MACHINE\Software\AppAssure\ReplayEPS\mailserver1
        # so let's split it to get just the server name 
        $tmparray=$Key.Name.Split("\")
        #Server's name Should be last element of the array
        $protectedserverName = $tmpArray[-1]

        $ArchivePath= $Key.GetValue("target_path")
        #Let's Report some info about the Archive Disk 
    
        Get-ProtectedServerDiskInfo $protectedserverName $ArchivePath

        Get-ReplayStatus $hostname $protectedServerName
    }

}
#######################################################################
# Main portion of script
#######################################################################
# Creates Timestamp and name of host
# Starts log collection
# Gathers whether or not Replay Service is running
# Gets list of protected servers
# Passes this list to Get-RollupStatistic to gather rollup settings
# Executes the Old Replay PS script. This was copied to its own
#      function to keep things neat.
# Generates subject for email depending on if there are contents in the
#      error log.
# Emails
#----------------------------------------------------------------------
$scriptStarttime = get-Date
$hostname = $env:Computername 
$Script:Log = "<html><head><title>Replay Report for $hostname at $ScriptStartTime</title></head>"
$Script:Log += "<p>Starting Script at $ScriptStartTime</P>`n"
Get-ReplayServiceStatus
Get-ProtectedServer
Get-RollupStatistic($global:ProtectedServers)
Get-ReplicationStatus($global:ProtectedServers)
Get-OldReplayPS1
$scriptEndTime = get-Date
$Script:Log += "<br><br><p>Ending Script at $ScriptStartTime</P>`n"
$mailSubject = "Replay Status Report for $hostname @ $ScriptStartTime"
If ($Script:ErrorLog.length -gt 0) {$mailSubject = "Replay ERROR Report for $hostname @ $ScriptStartTime"}
Send-MailMessage -To $sendmailto -From $mailfromaddress -Subject $mailSubject -Body $Log -SmtpServer $smtpserver -BodyAsHtml