#requires -version 3
#requires -RunAsAdministrator
#Requires -Modules AppAssurePowerShellModule

<#
.SYNOPSIS
  Changes settings on AppAssure5 Core

.DESCRIPTION
  Modifies settings on an AppAssure 5 Core for Client Timeout Settings, number of streams,
  timeouts and caching policies. Deploy to all cores and schedule run to synchronize settings
  across all companies as necessary tweaks for efficiency are discovered. Also useful for after
  installation of AppAssure 5 core software to get the backup core within configuration guide-
  lines.

.PARAMETERS
  No parameters required.

.INPUTS
  None

.OUTPUTS
  None

.NOTES
  Version:        1.0
  Author:         Taylor K. Zuppan
  Creation Date:  February 11, 2015
  Purpose/Change: Initial script development
  
.EXAMPLE
  <Example goes here. Repeat this attribute for more than one example>
#>

#---------------------------------------------------------[Initialisations]--------------------------------------------------------

#Set Error Action to Silently Continue
$ErrorActionPreference = "SilentlyContinue"

#Dot Source required Function Libraries


#----------------------------------------------------------[Declarations]----------------------------------------------------------

#Script Version
$sScriptVersion = "1.0"

#Log File Info

#Sets log path as PWD
$sLogPath = Split-Path -Parent -Path $MyInvocation.MyCommand.Definition
$scriptName = $MyInvocation.MyCommand.Name
$scriptName = $scriptName.TrimEnd(".ps1")
$sLogDate = get-date -f dd-MM-yyyy_HH_mm_ss
#Sets log file name to : $ScriptName_$Date.log
$sLogName = $scriptName + "_" + $sLogDate + ".log"
$sLogFile = $sLogPath + "\" + $sLogName

#Global Variables
$global:CoreRepositories = @()

#Outgoing Replication Server
$ReplicationDest = "r5.atechaz.com"

#Client Timeout Settings
$ConnectionTimeout = 00:15:00 #hh:mm:ss
$ReadWriteTimeout = 00:15:00 #hh:mm:ss
$ConnectionUITimeout = 00:15:00 #hh:mm:ss
$ReadWriteUITimeout = 00:15:00 #hh:mm:ss

#Replication Service Settings
$MaxParallelStreams = 4
$RemoteReplicationSyncJobTimeout = 00:30:00 #hh:mm:ss
$VolumeImageSessionTimeout = 01:00:00 #hh:mm:ss

#AppAssure Vdisk Parameters - All times in milliseconds
$VdiskAsyncNetworkTimeout = 1200000
$VdiskNetworkTimeout = 600000

#AppAssure VStor Parameters - Times in milliseconds
$VStorNetworkTimeout = 600000

#AppAssure WriteCachingPolicy for Extents
$WriteCachingPolicy = 3

#AppAssure AllocationPolicy for each repository
$AllocationPolicy = 1

#AppAssure MountabilityCheckingProcessTimeoutMinutes
$MountabilityCheckingProcessTimeoutMinutes = 1440

#-----------------------------------------------------------[Functions]------------------------------------------------------------
Function Log-Start{
  <#
  .SYNOPSIS
    Creates log file

  .DESCRIPTION
    Creates log file with path and name that is passed. Checks if log file exists, and if it does deletes it and creates a new one.
    Once created, writes initial logging data

  .PARAMETER LogPath
    Mandatory. Path of where log is to be created. Example: C:\Windows\Temp

  .PARAMETER LogName
    Mandatory. Name of log file to be created. Example: Test_Script.log
      
  .PARAMETER ScriptVersion
    Mandatory. Version of the running script which will be written in the log. Example: 1.5

  .INPUTS
    Parameters above

  .OUTPUTS
    Log file created

  .NOTES
    Version:        1.0
    Author:         Luca Sturlese
    Creation Date:  10/05/12
    Purpose/Change: Initial function development

    Version:        1.1
    Author:         Luca Sturlese
    Creation Date:  19/05/12
    Purpose/Change: Added debug mode support

  .EXAMPLE
    Log-Start -LogPath "C:\Windows\Temp" -LogName "Test_Script.log" -ScriptVersion "1.5"
  #>
    
  [CmdletBinding()]
  
  Param (
    [Parameter(Mandatory=$true)][string]$LogPath, 
    [Parameter(Mandatory=$true)][string]$LogName, 
    [Parameter(Mandatory=$true)][string]$ScriptVersion
  )
  
  Process{
    $sFullPath = $LogPath + "\" + $LogName
    
    #Check if file exists and delete if it does
    If((Test-Path -Path $sFullPath)){
      Remove-Item -Path $sFullPath -Force
    }
    
    #Create file and start logging
    New-Item -Path $LogPath -Value $LogName -ItemType File
    
    Add-Content -Path $sFullPath -Value "***************************************************************************************************"
    Add-Content -Path $sFullPath -Value "Started processing at [$([DateTime]::Now)]."
    Add-Content -Path $sFullPath -Value "***************************************************************************************************"
    Add-Content -Path $sFullPath -Value ""
    Add-Content -Path $sFullPath -Value "Running script version [$ScriptVersion]."
    Add-Content -Path $sFullPath -Value ""
    Add-Content -Path $sFullPath -Value "***************************************************************************************************"
    Add-Content -Path $sFullPath -Value ""
  
    #Write to screen for debug mode
    Write-Debug "***************************************************************************************************"
    Write-Debug "Started processing at [$([DateTime]::Now)]."
    Write-Debug "***************************************************************************************************"
    Write-Debug ""
    Write-Debug "Running script version [$ScriptVersion]."
    Write-Debug ""
    Write-Debug "***************************************************************************************************"
    Write-Debug ""
  }
}
Function Log-Write{
  <#
  .SYNOPSIS
    Writes to a log file

  .DESCRIPTION
    Appends a new line to the end of the specified log file
  
  .PARAMETER LogPath
    Mandatory. Full path of the log file you want to write to. Example: C:\Windows\Temp\Test_Script.log
  
  .PARAMETER LineValue
    Mandatory. The string that you want to write to the log
      
  .INPUTS
    Parameters above

  .OUTPUTS
    None

  .NOTES
    Version:        1.0
    Author:         Luca Sturlese
    Creation Date:  10/05/12
    Purpose/Change: Initial function development
  
    Version:        1.1
    Author:         Luca Sturlese
    Creation Date:  19/05/12
    Purpose/Change: Added debug mode support

  .EXAMPLE
    Log-Write -LogPath "C:\Windows\Temp\Test_Script.log" -LineValue "This is a new line which I am appending to the end of the log file."
  #>
  
  [CmdletBinding()]
  
  Param (
    [Parameter(Mandatory=$true)][string]$LogPath, 
    [Parameter(Mandatory=$true)][string]$LineValue
  )
  
  Process{
    Add-Content -Path $LogPath -Value $LineValue
  
    #Write to screen for debug mode
    Write-Debug $LineValue
  }
}
Function Log-Error{
  <#
  .SYNOPSIS
    Writes an error to a log file

  .DESCRIPTION
    Writes the passed error to a new line at the end of the specified log file
  
  .PARAMETER LogPath
    Mandatory. Full path of the log file you want to write to. Example: C:\Windows\Temp\Test_Script.log
  
  .PARAMETER ErrorDesc
    Mandatory. The description of the error you want to pass (use $_.Exception)
  
  .PARAMETER ExitGracefully
    Mandatory. Boolean. If set to True, runs Log-Finish and then exits script

  .INPUTS
    Parameters above

  .OUTPUTS
    None

  .NOTES
    Version:        1.0
    Author:         Luca Sturlese
    Creation Date:  10/05/12
    Purpose/Change: Initial function development
    
    Version:        1.1
    Author:         Luca Sturlese
    Creation Date:  19/05/12
    Purpose/Change: Added debug mode support. Added -ExitGracefully parameter functionality

  .EXAMPLE
    Log-Error -LogPath "C:\Windows\Temp\Test_Script.log" -ErrorDesc $_.Exception -ExitGracefully $True
  #>
  
  [CmdletBinding()]
  
  Param (
    [Parameter(Mandatory=$true)][string]$LogPath, 
    [Parameter(Mandatory=$true)][string]$ErrorDesc, 
    [Parameter(Mandatory=$true)][boolean]$ExitGracefully
  )
  
  Process{
    Add-Content -Path $LogPath -Value "Error: An error has occurred [$ErrorDesc]."
  
    #Write to screen for debug mode
    Write-Debug "Error: An error has occurred [$ErrorDesc]."
    
    #If $ExitGracefully = True then run Log-Finish and exit script
    If ($ExitGracefully -eq $True){
      Log-Finish -LogPath $LogPath
      Brea�k
    }
  }
}
Function Log-Finish{
  <#
  .SYNOPSIS
    Write closing logging data & exit

  .DESCRIPTION
    Writes finishing logging data to specified log and then exits the calling script
  
  .PARAMETER LogPath
    Mandatory. Full path of the log file you want to write finishing data to. Example: C:\Windows\Temp\Test_Script.log

  .PARAMETER NoExit
    Optional. If this is set to True, then the function will not exit the calling script, so that further execution can occur
  
  .INPUTS
    Parameters above

  .OUTPUTS
    None

  .NOTES
    Version:        1.0
    Author:         Luca Sturlese
    Creation Date:  10/05/12
    Purpose/Change: Initial function development
    
    Version:        1.1
    Author:         Luca Sturlese
    Creation Date:  19/05/12
    Purpose/Change: Added debug mode support
  
    Version:        1.2
    Author:         Luca Sturlese
    Creation Date:  01/08/12
    Purpose/Change: Added option to not exit calling script if required (via optional parameter)

  .EXAMPLE
    Log-Finish -LogPath "C:\Windows\Temp\Test_Script.log"

.EXAMPLE
    Log-Finish -LogPath "C:\Windows\Temp\Test_Script.log" -NoExit $True
  #>
  
  [CmdletBinding()]
  
  Param (
    [Parameter(Mandatory=$true)][string]$LogPath, 
    [Parameter(Mandatory=$false)][string]$NoExit
  )
  
  Process{
    Add-Content -Path $LogPath -Value ""
    Add-Content -Path $LogPath -Value "***************************************************************************************************"
    Add-Content -Path $LogPath -Value "Finished processing at [$([DateTime]::Now)]."
    Add-Content -Path $LogPath -Value "***************************************************************************************************"
  
    #Write to screen for debug mode
    Write-Debug ""
    Write-Debug "***************************************************************************************************"
    Write-Debug "Finished processing at [$([DateTime]::Now)]."
    Write-Debug "***************************************************************************************************"
  
    #Exit calling script if NoExit has not been specified or is set to False
    If(!($NoExit) -or ($NoExit -eq $False)){
      Exit
    }    
  }
}
Function Log-Email{
  <#
  .SYNOPSIS
    Emails log file to list of recipients

  .DESCRIPTION
    Emails the contents of the specified log file to a list of recipients
  
  .PARAMETER LogPath
    Mandatory. Full path of the log file you want to email. Example: C:\Windows\Temp\Test_Script.log
  
  .PARAMETER EmailFrom
    Mandatory. The email addresses of who you want to send the email from. Example: "admin@9to5IT.com"

  .PARAMETER EmailTo
    Mandatory. The email addresses of where to send the email to. Seperate multiple emails by ",". Example: "admin@9to5IT.com, test@test.com"
  
  .PARAMETER EmailSubject
    Mandatory. The subject of the email you want to send. Example: "Cool Script - [" + (Get-Date).ToShortDateString() + "]"

  .INPUTS
    Parameters above

  .OUTPUTS
    Email sent to the list of addresses specified

  .NOTES
    Version:        1.0
    Author:         Luca Sturlese
    Creation Date:  05.10.12
    Purpose/Change: Initial function development

  .EXAMPLE
    Log-Email -LogPath "C:\Windows\Temp\Test_Script.log" -EmailFrom "admin@9to5IT.com" -EmailTo "admin@9to5IT.com, test@test.com" -EmailSubject "Cool Script - [" + (Get-Date).ToShortDateString() + "]"
  #>
  
  [CmdletBinding()]
  
  Param (
    [Parameter(Mandatory=$true)][string]$LogPath, 
    [Parameter(Mandatory=$true)][string]$EmailFrom, 
    [Parameter(Mandatory=$true)][string]$EmailTo, 
    [Parameter(Mandatory=$true)][string]$EmailSubject
  )
  
  Process{
    Try{
      $sBody = (Get-Content $LogPath | out-string)
      
      #Create SMTP object and send email
      $sSmtpServer = "smtp.yourserver"
      $oSmtp = new-object Net.Mail.SmtpClient($sSmtpServer)
      $oSmtp.Send($EmailFrom, $EmailTo, $EmailSubject, $sBody)
      Exit 0
    }
    
    Catch{
      Exit 1
    } 
  }
}

#Gracefully Shutdown Core  -  https://support.software.dell.com/kb/119400
Function Stop-AA5CoreService{
  Param()
  
  Begin{
    Log-Write -LogPath $sLogFile -LineValue "Stopping AppAssureCore services..."
  }
  
  Process{
    Try{
      #Dismount all mounted recovery points
      Log-Write -LogPath $sLogFile -LineValue "Removing all mounted recovery points..."
      Remove-Mounts
      #Suspend all snapshots
      Log-Write -LogPath $sLogFile -LineValue "Suspending all snapshots..."
      Suspend-Snapshot -All
      #Suspend replication to the r5 core at PhxNAP
      Log-Write -LogPath $sLogFile -LineValue "Suspending outgoing replication to $ReplicationDest..."
      Suspend-Replication -Outgoing $ReplicationDest -All
      #Suspend continual VM exports
      Log-Write -LogPath $sLogFile -LineValue "Suspending virtual standby exports..."
      Suspend-VMexport -All
      #Set AA5 Service to disabled so it doesn't restart by itself
      Log-Write -LogPath $sLogFile -LineValue "Disabling AppAssureCore service..."
      Set-Service AppAssureCore -StartupType Disabled
      #Stop the AA5 service
      Log-Write -LogPath $sLogFile -LineValue "Stopping AppAssureCore service..."
      Stop-Service AppAssureCore
      #Test to see if Core.Service is still running. If so, force kill it.
      Get-Process Core.Service
      if($?) {
        Log-Write -LogPath $sLogFile -LineValue "Core.Service still running. Force terminating process..."
        Stop-Process -Processname Core.Service -Force
      }
    }
    
    Catch{
      Log-Error -LogPath $sLogFile -ErrorDesc $_.Exception -ExitGracefully $True
      Break
    }
  }
  
  End{
    If($?){
      Log-Write -LogPath $sLogFile -LineValue "AppAssureCore services gracefully halted."
      Log-Write -LogPath $sLogFile -LineValue " "
    }
  }
}

#Start Core Services after gracefully having shut them down.
Function Start-AA5CoreService{
  Param()
  
  Begin{
    Log-Write -LogPath $sLogFile -LineValue "Enabling AppAssureCore service and resuming suspended tasks..."
  }
  
  Process{
    Try{
      #Enables the core service
      Log-Write -LogPath $sLogFile -LineValue "Enabling AppAssureCore service and setting to Automatic(Delayed)..."
      Sc.exe Config AppAssureCore Start= Delayed-Auto
      #Starts the core service
      Log-Write -LogPath $sLogFile -LineValue "Starting AppAssureCore service..."
      Start-Service AppAssureCore
      #Resumes snapshots
      Log-Write -LogPath $sLogFile -LineValue "Resuming snapshots..."
      Resume-Snapshot -All
      #Resumes replication
      Log-Write -LogPath $sLogFile -LineValue "Resuming replication to $ReplicationDest..."
      Resume-Replication -Outgoing $ReplicationDest -All
      #Resume VM standby
      Log-Write -LogPath $sLogFile -LineValue "Resuming virtual standby exports..."
      Resume-Vmexport -All
    }
    
    Catch{
      Log-Error -LogPath $sLogFile -ErrorDesc $_.Exception -ExitGracefully $True
      Break
    }
  }
  
  End{
    If($?){
      Log-Write -LogPath $sLogFile -LineValue "Enabled AppAssureCore service and resumed tasks successfully."
      Log-Write -LogPath $sLogFile -LineValue " "
    }
  }
}

#Sets the AA5 Core's Client Timeout Settings for the following: Connection, ReadWrite,
#ConnectionUI, and ReadWriteUI as specified in the declarations.
Function Set-ClientTimeoutSettings($ConnectionTimeout,$ReadWriteTimeout,$ConnectionUITimeout,$ReadWriteUITimeout){
  Param()
  
  Begin{
    Log-Write -LogPath $sLogFile -LineValue "Setting the following registry keys:`r`n  HKEY_LOCAL_MACHINE\SOFTWARE\AppRecovery\Core\CoreSettings\ClientTimeoutSettings`r`n    ConnectionTimeout   =  $ConnectionTimeout`r`n    ReadWriteTimeout    =  $ReadWriteTimeout`r`n    ConnectionUITimeout =  $ConnectionUITimeout`r`n    ReadWriteUITimeout  =  $ReadWriteUITimeout"
  }
  
  Process{
    Try{
      #Sets the following registry keys:
      #  HKEY_LOCAL_MACHINE\SOFTWARE\AppRecovery\Core\CoreSettings\ClientTimeoutSettings
      #    ConnectionTimeout = $ConnectionTimeout
      #    ReadWriteTimeout = $ReadWriteTimeout
      #    ConnectionUITimeout = $ConnectionUITimeout
      #    ReadWriteUITimeout = $ReadWriteUITimeout
      $RootKey = "HKLM:\SOFTWARE\AppRecovery\Core\CoreSettings\ClientTimeoutSettings"
      Set-ItemProperty -Path $RootKey -Name ConnectionTimeout -Value $ConnectionTimeout
      Set-ItemProperty -Path $RootKey -Name ReadWriteTimeout -Value $ReadWriteTimeout
      Set-ItemProperty -Path $RootKey -Name ConnectionUITimeout -Value $ConnectionUITimeout
      Set-ItemProperty -Path $RootKey -Name ReadWriteUITimeout -Value $ReadWriteUITimeout
    }
    
    Catch{
      Log-Error -LogPath $sLogFile -ErrorDesc $_.Exception -ExitGracefully $True
      Break
    }
  }
  
  End{
    If($?){
      Log-Write -LogPath $sLogFile -LineValue "ClientTimeout settings configured successfully."
      Log-Write -LogPath $sLogFile -LineValue " "
    }
  }
}

#Sets the AA5 Core's Maximum number of Parallel Streams, Remote Sync timeout, and
#volume image session timeouts as specified in the declarations.
Function Set-ReplicationService($MaxParallelStreams,$RemoteReplicationSyncJobTimeout,$VolumeImageSessionTimeout){
  Param()
  
  Begin{
    Log-Write -LogPath $sLogFile -LineValue "Setting the following registry keys:`r`n  HKEY_LOCAL_MACHINE\SOFTWARE\AppRecovery\Core\Replication\ReplicationService`r`n    MaxParallelStreams               =  $MaxParallelStreams`r`n    RemoteReplicationSyncJobTimeout  =  $RemoteReplicationSyncJobTimeout`r`n    VolumeImageSessionTimeout        =  $VolumeImageSessionTimeout"
  }
  
  Process{
    Try{
      #Sets the following registry keys:
      #  HKEY_LOCAL_MACHINE\SOFTWARE\AppRecovery\Core\Replication\ReplicationService
      #    MaxParallelStreams = $MaxParallelStreams
      #    RemoteReplicationSyncJobTimeout = $RemoteReplicationSyncJobTimeout
      #    VolumeImageSessionTimeout = $VolumeImageSessionTimeout
      $RootKey = "HKLM:\SOFTWARE\AppRecovery\Core\Replication\ReplicationService"
      Set-ItemProperty -Path $RootKey -Name MaxParallelStreams -Value $MaxParallelStreams
      Set-ItemProperty -Path $RootKey -Name RemoteReplicationSyncJobTimeout -Value $RemoteReplicationSyncJobTimeout
      Set-ItemProperty -Path $RootKey -Name VolumeImageSessionTimeout -Value $VolumeImageSessionTimeout
    }
    
    Catch{
      Log-Error -LogPath $sLogFile -ErrorDesc $_.Exception -ExitGracefully $True
      Break
    }
  }
  
  End{
    If($?){
      Log-Write -LogPath $sLogFile -LineValue "ReplicationService settings configured successfully."
      Log-Write -LogPath $sLogFile -LineValue " "
    }
  }
}

#Sets the AA5 Core's Vdisk Parameters as specified in the declarations
Function Set-VdiskParameters($VdiskAsyncNetworkTimeout,$VdiskNetworkTimeout){
  Param()
  
  Begin{
    Log-Write -LogPath $sLogFile -LineValue "Setting the following registry keys:`r`n  HKEY_LOCAL_MACHINE\System\CurrentControlSet\Services\AAVdisk\Parameters`r`n    AsyncNetworkTimeout  =  $VdiskAsyncNetworkTimeout`r`n    NetworkTimeout       =  $VdiskNetworkTimeout"
  }
  
  Process{
    Try{
      #Sets the following registry keys:
      #  HKEY_LOCAL_MACHINE\System\CurrentControlSet\Services\AAVdisk\Parameters
      #    AsyncNetworkTimeout = $VdiskAsyncNetworkTimeout
      #    NetworkTimeout = $VdiskNetworkTimeout
      $RootKey = "HKLM:\System\CurrentControlSet\Services\AAVdisk\Parameters"
      Set-ItemProperty -Path $RootKey -Name AsyncNetworkTimeout -Value $VdiskAsyncNetworkTimeout
      Set-ItemProperty -Path $RootKey -Name NetworkTimeout -Value $VdiskNetworkTimeout
    }
    
    Catch{
      Log-Error -LogPath $sLogFile -ErrorDesc $_.Exception -ExitGracefully $True
      Break
    }
  }
  
  End{
    If($?){
      Log-Write -LogPath $sLogFile -LineValue "AAVdisk parameters set successfully."
      Log-Write -LogPath $sLogFile -LineValue " "
    }
  }
}

#Sets the AA5 Core's VStor Parameters as specified in the declarations
Function Set-VStorParameters($VStorNetworkTimeout){
  Param()
  
  Begin{
    Log-Write -LogPath $sLogFile -LineValue "Sets the following registry keys:`r`n  HKEY_LOCAL_MACHINE\System\CurrentControlSet\Services\AAVStor\Parameters`r`n    NetworkTimeout = $VStorNetworkTimeout"
  }
  
  Process{
    Try{
      #Sets the following registry keys:
      #  HKEY_LOCAL_MACHINE\System\CurrentControlSet\Services\AAVStor\Parameters
      #    NetworkTimeout = $VStorNetworkTimeout
      $RootKey = "HKLM:\System\CurrentControlSet\Services\AAVStor\Parameters"
      Set-ItemProperty -Path $RootKey -Name NetworkTimeout -Value $VStorNetworkTimeout
    }
    
    Catch{
      Log-Error -LogPath $sLogFile -ErrorDesc $_.Exception -ExitGracefully $True
      Break
    }
  }
  
  End{
    If($?){
      Log-Write -LogPath $sLogFile -LineValue "AAVStor parameters set successfully."
      Log-Write -LogPath $sLogFile -LineValue " "
    }
  }
}

#Retrieves OS Version
Function Get-OSVersion{
  Param()

  Begin{
    Log-Write -LogPath $sLogFile -LineValue "Retrieving OS version...`r`n  Version number dictates OS as follows:`r`n    5.0.x = Server 2000`r`n    5.1.x = XP`r`n    5.2.x = Server 2003/R2, XP Pro x64`r`n    6.0.x = Server 2008, Vista`r`n    6.1.x = Server 2008 R2, Windows 7`r`n    6.2.x = Server 2012, Windows 8`r`n    6.3.x = Server 2012 R2, Windows 8.1"
  }
  
  Process{
    Try{
      <#
        5.0.x = Server 2000
        5.1.x = XP
        5.2.x = Server 2003/R2, XP Pro x64
        6.0.x = Server 2008, Vista
        6.1.x = Server 2008 R2, Windows 7
        6.2.x = Server 2012, Windows 8
        6.3.x = Server 2012 R2, Windows 8.1
      #>  
      $OSversion = Get-WmiObject Win32_OperatingSystem | Select Version
      $OSversion = $OSversion.Version
      Log-Write -LogPath $sLogFile -LineValue "OS Version: $OSVersion"
      return $OSversion
    }
    
    Catch{
      Log-Error -LogPath $sLogFile -ErrorDesc $_.Exception -ExitGracefully $True
      Break
    }
  }
  
  End{
    If($?){
      Log-Write -LogPath $sLogFile -LineValue "Retrieved OS version successfully."
      Log-Write -LogPath $sLogFile -LineValue " "
    }
  }
}

#Retrieves the UID for each repository on the AA5 Core
Function Get-CoreRepositories{
  Param()
  
  Begin{
    Log-Write -LogPath $sLogFile -LineValue "Retrieving repositories..."
  }
  
  Process{
    Try{
      #Registry Key where AA5 Repositories are supposed to be located
	  $RepositoryKey= "HKLM:\Software\AppRecovery\Core\Repositories"
	  #Get an array of the subkeys... should be one for each repository
	  $Keys = Get-ChildItem $RepositoryKey
      #Create an empty array to house the list of repositories
	  Foreach ($Key in $Keys) {
	      #$Key.name is a value like: HKEY_LOCAL_MACHINE\AppRecovery\Core\Repositories\c70c8fcf-7eeb-4cf5-9cd7-5766332c8f50
   	      # so let's split it to get just the server name 
	      $tmparray=$Key.Name.Split("\")
	      #Repository UID should be last element of the array
	      $RepositoryUID = $tmpArray[-1]
          Log-Write -LogPath $sLogFile -LineValue "Adding $RepositoryUID to list of repositories..."
          $global:CoreRepositories +=  $RepositoryUID
      }
    }
    
    Catch{
      Log-Error -LogPath $sLogFile -ErrorDesc $_.Exception -ExitGracefully $True
      Break
    }
  }
  
  End{
    If($?){
      Log-Write -LogPath $sLogFile -LineValue "Successfully retrieved listing of all repositories..."
      Log-Write -LogPath $sLogFile -LineValue " "
    }
  }
}

#Sets the AA5 Core's WriteCachingPolicy to each extent of each repository
#Get-CoreRepositories must be ran first
Function Set-WriteCachingPolicy($Repositories){
  Param()
  
  Begin{
    Log-Write -LogPath $sLogFile -LineValue "Setting WriteCachingPolicy for each extent of each repository..."
  }
  
  Process{
    Try{
      $Extents = @()
      ForEach($Repository in $Repositories) {
        $ExtentKey = "HKLM:\SOFTWARE\AppRecovery\Core\Repositories\$Repository\FileConfigurations"
        $Keys = Get-ChildItem $ExtentKey
        ForEach($Key in $Keys) {
            $tmparray=$Key.Name.Split("\")
            $ExtentNumber = $tmparray[-1]
            $Extents += $ExtentNumber
        }
        ForEach($Extent in $Extents) {
            $RootKey = "HKLM:\SOFTWARE\AppRecovery\Core\Repositories\$Repository\FileConfigurations\$Extent\Specification"
            Set-ItemProperty -Path $RootKey -Name WriteCachingPolicy -Value $WriteCachingPolicy
        }
      }
    }
    
    Catch{
      Log-Error -LogPath $sLogFile -ErrorDesc $_.Exception -ExitGracefully $True
      Break
    }
  }
  
  End{
    If($?){
      Log-Write -LogPath $sLogFile -LineValue "Successfully set WriteCachingPolicy for each extent."
      Log-Write -LogPath $sLogFile -LineValue " "
    }
  }
}

#Sets the allocation policy
#Directs the repository to use a sequential writing policy
#Make the change if the extents live on the same volume
#For SAN storage, this tells AA to use each storage location
#sequentially one at a time.
#
#Reduces the chance for split I/O in the SAN
Function Set-AllocationPolicy($Repositories){
  Param()
  
  Begin{
    Log-Write -LogPath $sLogFile -LineValue "Settings AllocationPolicy for each repository..."
  }
  
  Process{
    Try{
      ForEach($Repository in $Repositories) {
        $RootKey = "HKLM:\SOFTWARE\AppRecovery\Core\Repositories\$Repository\Specification"
        Set-ItemProperty -Path $RootKey -Name AllocationPolicy -Value $AllocationPolicy
      }
    }
    
    Catch{
      Log-Error -LogPath $sLogFile -ErrorDesc $_.Exception -ExitGracefully $True
      Break
    }
  }
  
  End{
    If($?){
      Log-Write -LogPath $sLogFile -LineValue "Successfully set AllocationPolicy for each repository."
      Log-Write -LogPath $sLogFile -LineValue " "
    }
  }
}

#Set-MountabilityCheckingProcessTimeout
Function Set-MountabilityCheckingProcessTimeout{
  Param()
  
  Begin{
    Log-Write -LogPath $sLogFile -LineValue "Setting MountabilityCheckingProcessTimeoutMinutes for ExchangeServiceConfiguration..."
  }
  
  Process{
    Try{
      $RootKey = "HKLM:\SOFTWARE\AppRecovery\Core\ExchangeServiceConfiguration"
      Set-ItemProperty -Path $RootKey -Name MountabilityCheckingProcessTimeoutMinutes -Value $MountabilityCheckingProcessTimeoutMinutes
    }
    
    Catch{
      Log-Error -LogPath $sLogFile -ErrorDesc $_.Exception -ExitGracefully $True
      Break
    }
  }
  
  End{
    If($?){
      Log-Write -LogPath $sLogFile -LineValue "Successfully set MountabilityCheckingProcessTimeoutMinutes."
      Log-Write -LogPath $sLogFile -LineValue " "
    }
  }
}

<#

Function <FunctionName>{
  Param()
  
  Begin{
    Log-Write -LogPath $sLogFile -LineValue "<description of what is going on>..."
  }
  
  Process{
    Try{
      <code goes here>
    }
    
    Catch{
      Log-Error -LogPath $sLogFile -ErrorDesc $_.Exception -ExitGracefully $True
      Break
    }
  }
  
  End{
    If($?){
      Log-Write -LogPath $sLogFile -LineValue "Completed Successfully."
      Log-Write -LogPath $sLogFile -LineValue " "
    }
  }
}

#>

#-----------------------------------------------------------[Execution]------------------------------------------------------------

#Log-Start -LogPath $sLogPath -LogName $sLogName -ScriptVersion $sScriptVersion
#Script Execution goes here

#If AppAssure Core is installed
if(Test-Path "C:\Program Files\AppRecovery\Core"){
  #Retrieve list of all repository GUIDs
  Get-CoreRepositories
  #Display repository GUIDs
  Write-Host $global:CoreRepositories
  #Gracefully shutdown core
  Stop-AA5CoreService
  #Begin configuring registry settings
  Set-ClientTimeoutSettings $ConnectionTimeout $ReadWriteTimeout $ConnectionUITimeout $ReadWriteUITimeout
  Set-ReplicationService $MaxParallelStreams $RemoteReplicationSyncJobTimeout $VolumeImageSessionTimeout
  Set-VdiskParameters $VdiskAsyncNetworkTimeout $VdiskNetworkTimeout
  Set-VStorParameters $VStorNetworkTimeout
  Set-WriteCachingPolicy $global:CoreRepositories
  Set-AllocationPolicy $global:CoreRepositories
  Set-MountabilityCheckingProcessTimeout
}

#If AppAssure Agent is installed
if(Test-Path "C:\Program Files\AppRecovery\Agent"){
}
Log-Finish -LogPath $sLogFile