#############################################################
# These variables need to be configured for your environment
#---------------------------------------------------------------

#Array of addresses that the report should go to...
$sendmailto = "tzuppan@domain.com

#SMTP server we should send mail through
$smtpserver = "mail.domain.com"

# The address the report should show as being from
$mailfromaddress = "replay@domain.com"
######################################################################
# Shouldn't need to touch anything below here
#

#######################################################################
# Global Variables necessary for subsequent functions
#----------------------------------------------------------------------
$Log = ""
$ErrorLog = ""

#######################################################################
# !!Needs $ProtectedServers defined outside function
# List protected volumes in an array
# For other functions perform a foreach($Server in $Servers)
#----------------------------------------------------------------------
$global:ProtectedServers = @()
function Get-ProtectedServer(){
	#Registry Key where Replay Info is supposed to be located
	$Replay_Key= "HKLM:\Software\AppAssure\ReplayEPS"
	#Get an array of the subkeys... should be one for each server being protected
	$Keys = Get-ChildItem $Replay_Key
    #Create an empty array to house the list of protected servers
	Foreach ($Key in $Keys) {
	    #$Key.name is a value like: HKEY_LOCAL_MACHINE\Software\AppAssure\ReplayEPS\mailserver1
	    # so let's split it to get just the server name 
	    $tmparray=$Key.Name.Split("\")
	    #Server's name Should be last element of the array
	    $protectedserverName = $tmpArray[-1]
        $global:ProtectedServers +=  $protectedserverName
    }
}
#######################################################################
# !!Needs: $ErrorLog defined outside function
# !!Needs: Get-ProtectedServer function to run prior
# !!Usage: Function must be called with Get-RollupStatistic($ProtectedServers)
# Retrieve all rollup settings for each server
#----------------------------------------------------------------------
$global:RollupStatistics = @()
function Get-RollupStatistic($Servers){
    Foreach ($Server in $Servers) {
        Try {
            $all = Get-ItemProperty -Path HKLM:\Software\AppAssure\ReplayEPS\$Server -name days_to_keep_all_snaps
            $hourly = Get-ItemProperty -Path HKLM:\Software\AppAssure\ReplayEPS\$Server -name days_to_keep_hourly_snaps
            $daily = Get-ItemProperty -Path HKLM:\Software\AppAssure\ReplayEPS\$Server -name days_to_keep_daily_snaps
            $weekly = Get-ItemProperty -Path HKLM:\Software\AppAssure\ReplayEPS\$Server -name weeks_to_keep_weekly_snaps
            $monthly = Get-ItemProperty -Path HKLM:\Software\AppAssure\ReplayEPS\$Server -name months_to_keep_monthly_snaps
            
            $object = New-Object -TypeName PSObject

            $object | Add-Member `
             -MemberType NoteProperty `
             -Name ServerName `
             -Value $Server

            $object | Add-Member  `
             -MemberType NoteProperty `
             -Name DaysToKeepAllSnaps `
             -Value $all.days_to_keep_all_snaps

             $object | Add-Member `
             -MemberType NoteProperty `
             -Name DaysToKeepHourlySnaps `
             -Value $hourly.days_to_keep_hourly_snaps

             $object | Add-Member `
             -MemberType NoteProperty `
             -Name DaysToKeepDailySnaps `
             -Value $daily.days_to_keep_daily_snaps

             $object | Add-Member `
             -MemberType NoteProperty `
             -Name WeeksToKeepWeeklySnaps `
             -Value $weekly.weeks_to_keep_weekly_snaps
         
             $object | Add-Member `
             -MemberType NoteProperty `
             -Name MonthsToKeepMonthlySnaps `
             -Value $monthly.months_to_keep_monthly_snaps
             
             $global:RollupStatistics += $object
             

        } Catch {
            $ErrorLog += "$Server encountered an error.`n"
        }
#        $global:RollupStatistics | Out-File -FilePath C:\temp\rollupstatistics.txt
    }
}

#######################################################################
# Mail portion of script
#######################################################################
# Gets list of all protected servers.
# Passes this to the rollup statistic function
# Writes the results of the Get-RollupStatistic function to a text file
# Emails the text file
# Deletes the text file to clean up after itself
#----------------------------------------------------------------------
Get-ProtectedServer
Get-RollupStatistic($global:ProtectedServers)
#Save results to text file for emailing as just placing the variable in the body
#causes loss of formatting.
$global:RollupStatistics | Out-File -FilePath C:\temp\rollupstatistics.txt
Send-MailMessage -To $sendmailto -From $mailfromaddress -Subject "Rollup Statistics for $env:ComputerName " -Attachments c:\temp\rollupstatistics.txt -SmtpServer $smtpserver
Remove-Item C:\temp\rollupstatistics.txt