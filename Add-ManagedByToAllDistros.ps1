<#
This will loop through all distro groups and add specified members
as managers.

To add members to all distribution groups, please modify the variable
$usernames below.


Taylor K. Zuppan
12.09.2014

##################
Updated 10/16/2014
by
TKZ
##################
Added PSSNAPIN for exchange commands, so can be run from normal powershell console.

Restructured the for loop to only require going through each group once.
Was previously looping through each distro group once for each user,
causing the run time to be unacceptable
#>

<#  Enter all USERNAMES of accounts to be added as managers within single quotes.
    Please place a comma and space after each USERNAME except for the last.
    i.e.
    $list = 'username1', 'username2', 'username3'
#>
$usernames = 'username1', 'username2', 'username3'

###########################################
## Do not edit anything below this line! ##
###########################################
Add-PSSnapin *exchange*  #Add the Exchange PowerShell Snapin so it doesn't have to be ran from the Exchange Management Shell
$Groups = Get-DistributionGroup #Retrieve a list of distribution groups

#Loop through each group, executing the following instructions
ForEach($Grp in $Groups) {
    Write-Host $Grp
	$List = $Grp.ManagedBy #Add current managers to variable $List
    Write-Host $List

    #Loop through each username in the above configured variable $usernames
    #Retrieve user info for each username, and add to the $List of managers
   	ForEach($username in $usernames) {
        $AddOn = Get-User $username
        $List += $AddOn
    }

    #After adding the users from the configured $usernames variable
    #to the current $List of managers, reapply this new, concatenated $List
    #to the Distro's ManagedBy attribute
	Set-DistributionGroup $Grp -ManagedBy $List
}